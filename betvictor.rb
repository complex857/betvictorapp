require 'json'
require "pp"
require 'net/http'

require 'sinatra'
require "sinatra/json"
require "sinatra/config_file"

require "./lib/sportsapi"
require "./lib/sportsdata"

class BetVictorTestApp < Sinatra::Base
  register Sinatra::JSON
  register Sinatra::ConfigFile

  config_file 'app_config.yml'

  set :public_folder , Proc.new { File.join(File.dirname(__FILE__), 'public') }
  enable :static

  def initialize
    super()
    @sports_apis = {}
  end

  before do
    params['lang'] = settings.default_lang unless params.has_key? 'lang'

    unless @sports_apis.has_key? params['lang']
      @sports_apis[params['lang']] = SportsApi.new(
        settings.api_url % {lang: params['lang']},
        {
          grace_time: settings.api_cache_grace_time
        }
      )
    end
  end

  get %r{/([a-z]{2})?$} do
    params['lang'] = params['captures'].first if params.has_key? 'captures'
    erb :index
  end

  get '/sports' do
    sports_data = SportsData.new(@sports_apis[params['lang']])
    json sports_data.sports
  end

  get '/sports/:sport_id' do |sport_id|
    sports_data = SportsData.new(@sports_apis[params['lang']])

    json({
      events: sports_data.events(sport_id),
      sport: sports_data.sport(sport_id),
    })
  end

  get '/sports/:sport_id/events/:event_id' do |sport_id, event_id|
    sports_data = SportsData.new(@sports_apis[params['lang']])

    json({
      sport: sports_data.sport(sport_id),
      event: sports_data.event(sport_id, event_id),
      outcomes: sports_data.outcomes(sport_id, event_id)
    })
  end

  not_found do
    "Sorry this can't be found. Details: " + env['sinatra.error'].message
  end

  run! if app_file == $0
end
