class SportsApi

  def initialize(url, options = {})
    @uri = URI(url)
    @cache = {}
    @sports = []
    @sports_by_id = {}
    @api_grace_time = options.fetch(:grace_time, 10)
  end

  def sports
    ensure_fresh_data

    {
      ordered: @sports,
      by_id:   @sports_by_id
    }
  end

  private

  def ensure_fresh_data
    unless cache_valid?
      response = fetch
      unless response.nil?
        @cache['headers'] = response.to_hash
        @cache['body'] = JSON.parse(response.body)
        @cache['last_fetch'] = DateTime.now
        process_response
      end
    end
  end

  def fetch
    req = Net::HTTP::Get.new(@uri.request_uri)

    if @cache.has_key? 'headers' and @cache['headers'].has_key? 'last-modified'
      req['If-Modified-Since'] = DateTime.parse(@cache['headers']['last-modified'][0]).rfc2822
    end

    res = Net::HTTP.start(@uri.hostname, @uri.port) do |http|
      http.request(req)
    end

    unless res.is_a?(Net::HTTPSuccess)
      return nil
    end

    res
  end

  def cache_valid?
    unless @cache.has_key? 'headers'
      return false
    end

    now = DateTime.now
    if @cache['headers'].has_key? 'expires'
      expires = DateTime.parse(@cache['headers']['expires'][0])

      return true if (expires > now)
    end

    # if the last fetch was less than a few seconds, we consider it fresh
    if @cache.has_key? 'last_fetch' and @cache['last_fetch'].to_time + @api_grace_time > now.to_time
      return true
    end

    false
  end

  def process_response
    return nil unless @cache.has_key? 'body'

    @sports = []
    @sports_by_id = {}

    sports_data = @cache['body']['sports'].sort_by { |sport| sport['pos'] }
    sports_data.each do |sport|
      sport['events'].sort_by! { |event| event['pos'] }

      events_by_id = {}
      sport['events'].each do |event|
        events_by_id[event['id']] = event
      end
      sport['events_by_id'] = events_by_id

      @sports.push(sport)
      @sports_by_id[sport['id']] = sport
    end
  end

end
