class SportsData

  def initialize(sports_api)
    sports = sports_api.sports
    @sports = sports[:ordered]
    @sports_by_id = sports[:by_id]
  end

  def sports
    @sports.map do |sport|
      sport.dup.keep_if do |key, value|
        not ['events', 'events_by_id'].include? key
      end
    end
  end

  def sport(sport_id)
    sport_id = Integer(sport_id)

    fail SportNotFound, "No sport with id: #{sport_id}" unless @sports_by_id.has_key? sport_id

    @sports_by_id[sport_id].dup.keep_if do |key, value|
      not ['events', 'events_by_id'].include? key
    end
  end

  def event(sport_id, event_id)
    sport_id = Integer(sport_id)
    event_id = Integer(event_id)

    fail SportNotFound, "No sport with id: #{sport_id}" unless @sports_by_id.has_key? sport_id
    fail EventNotFound, "No event with id: #{event_id}" unless @sports_by_id[sport_id]['events_by_id'].has_key? event_id

    @sports_by_id[sport_id]['events_by_id'][event_id]
  end

  def events(sport_id)
    sport_id = Integer(sport_id)

    fail SportNotFound, "No sport with id: #{sport_id}" unless @sports_by_id.has_key? sport_id

    @sports_by_id[sport_id]['events']
  end

  def outcomes(sport_id, event_id)
    sport_id = Integer(sport_id)
    event_id = Integer(event_id)

    fail SportNotFound, "No sport with id: #{sport_id}" unless @sports_by_id.has_key? sport_id
    fail EventNotFound, "No event with id: #{event_id}" unless @sports_by_id[sport_id]['events_by_id'].has_key? event_id

    @sports_by_id[sport_id]['events_by_id'][event_id]['outcomes']
  end

end

class SportNotFound < Sinatra::NotFound
end
class EventNotFound < Sinatra::NotFound
end
