BetVictor Interview Task
========================

# Install
Gem dependencies are handled with `bundler` if you don't have it already installed, run `gem install bundler` first.
Then, run `bundle install` to get the required gems for the app.
Rackup config is provided so you can start the application with the `rackup` command, or with `ruby betvictor.rb` to start the app.
This will most likely spin up a `WEBRick` webserver on `localhost:9292` so you point your browser to that address to access the app (the address will be printed on your console).

I've also put the app here: http://betvictor.mahou.moe/ so you can give it a spin there.

# Tests
Tests are set up with rspec, you can run `rspec` in the root of the repository.
For convinience coverage report is generated with the `simplecov` gem, check under coverage/ folder after the first test run.

# Notes 
There's two things I've found with the json data that worth a note:

 1. The `expires` HTTP header seems to be always one hour before the request is submitted (compared to the `date` HTTP header in the same response). I assume that this feed is really volatile and changes a lot but for forbidding caching theres the `Cache-Control: no-cache` or `Pragma: no-cache`. This is especially strange since you have a `last-modified` header too on your responses but with this setup that's useless.
 2. Some of the `event` don't seem to have the `time` property while others do. I'm not sure why.
