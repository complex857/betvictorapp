/*jslint continue: true, debug: false, eqeq: true, regexp: true, sloppy: true, vars: true, white: true, browser: true, devel: true, indent: 4, es5: false, nomen: true */
/*global jQuery, _, $, Backbone, i18next, i18n */

window.app = window.app || {};

var TableBuilder = function(opts){
    this.data = opts.data || [];
    this.headers = opts.headers || [];
    this.formatters = opts.formatters || {};
};

TableBuilder.prototype.build = function(){
    var self = this;
    var thead = [];
    var tbody = [];

    if (self.data.length === 0) {
        return '<tr><td><em class="block center">'+i18n.t('Not found')+'</em></td></tr>';
    }

    if (self.headers === 'auto') {
        _.each(_.keys(self.data[0].attributes), function(key){
            thead.push('<th>'+self.autoHeader(key)+'</th>');
        });
    } else {
        _.each(self.headers, function(header){
            thead.push('<th>'+header[1]+'</th>');
        });
    }

    _.each(self.data, function(row){
        tbody.push('<tr>');
        if (self.headers === 'auto') {
            _.each(row.attributes, function(value, key) {
                if (typeof self.formatters[key] === 'function') {
                    value = self.formatters[key](row);
                }

                tbody.push('<td>'+self.autoFormatter(value)+'</td>');
            });
        } else {
            _.each(self.headers, function(header){
                var key = header[0];
                var name = header[1];
                var value = row.get(key);

                if (typeof self.formatters[name] === 'function') {
                    value = self.formatters[name](row);
                }
                tbody.push('<td>'+self.autoFormatter(value)+'</td>');
            });
        }
        tbody.push('</tr>');
    });

    return '<thead>'+ thead.join('')+ '</thead>'+
           '<tbody>'+ tbody.join('')+ '</tbody>';
};

TableBuilder.prototype.autoHeader = function(key){
    var postfix = '';
    var re = key;

    if (/^is_/.test(re)) {
        postfix = '?';
        re = re.replace(/^is_/, '');
    }

	/*jslint unparam: true*/
    re = re.replace(/_([a-z])/g, function(full, match_1){
        return ' '+match_1.toUpperCase();
    });
	/*jslint unparam: false*/


    re = i18n.t(re);

    re = re.replace(/^[a-z]/, function(match){
        return match.toUpperCase();
    });

    return re + postfix;
};

TableBuilder.prototype.autoFormatter = function(value){
    if (value === null) {
        return '<span class="alpha50 it">'+i18n.t('none')+'</span>';
    }
    if (value === true || value === false) {
        return value ? i18n.t('yes') : i18n.t('no');
    }
    return value;
};


$(function () {
	'use strict';

    var lang_loaded = $.Deferred();

    i18next
    .use(window.i18nextXHRBackend)
    .init({
        backend: {
            loadPath: '/locales/{{lng}}.json'
        },
        lng: window.BACKEND_LANG,
        debug: false,
        fallbackLng: 'en'
    }, function(){
        window.i18n = window.i18next;
        lang_loaded.resolve(true);
    });

    lang_loaded.done(function(){
        var app = window.app;
        app.breadcrumbs = new app.BreadcrumbsView();
        app.langselect = new app.LangSelectView();
        app.pageloader = new app.LoaderView();

        var AppRouter = Backbone.Router.extend({
            routes: {
                "sports/:sport_id":                  "events",
                "sports/:sport_id/events/:event_id": "outcomes",
                "*actions":                          "default"
            }
        });

        // Initiate the router
        var app_router = new AppRouter();

        app_router.on('route:default', function() {
            return new app.AppView();
        });

        app_router.on('route:events', function(sport_id) {
            return new app.EventView(sport_id);
        });

        app_router.on('route:outcomes', function(sport_id, event_id) {
            return new app.OutcomesView(sport_id, event_id);
        });

        Backbone.history.start();
    });

});

