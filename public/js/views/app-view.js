/*jslint continue: true, debug: false, eqeq: true, regexp: true, sloppy: true, vars: true, white: true, browser: true, devel: true, indent: 4, es5: false, nomen: true */
/*global jQuery, Backbone, $, _, TableBuilder, i18n */

window.app = window.app || {};

(function ($) {
	'use strict';

	var app = window.app;

	app.AppView = Backbone.View.extend({
		el: $('#content'),

		sports: null,
		headers: [],

		initialize: function(){
			var self = this;
			self.sports = new app.Sports();
			self.headers = [['id', 'ID'], ['title', i18n.t('name')], ['is_virtual', i18n.t('virtual')+'?'], ['id', i18n.t('events')]];

			self.render();
		},
		render: function(){
			var self = this;

			app.breadcrumbs.render('default');
			
			app.pageloader.show();

			$.get('/sports', {lang: app.langselect.selected_lang}).done(function(resp){
				_.each(resp, function(sport_data){
					self.sports.push(new app.Sport(sport_data));
				});

				var formatters = {};
				formatters[i18n.t('events')] = function(sport) {
					var id = sport.get('id');
					return '<a class="block" href="#/sports/'+id+'">'+i18n.t('events')+'</a>';
				};
				var table_builder = new TableBuilder({
					data: self.sports.models,
					headers: self.headers,
					formatters: formatters
				});
				var table_html = table_builder.build();

				var html =
					'<table id="sports" class="table table-condensed table-bordered table-hover">'+
					table_html+
					'</table>';

				self.$el.html(html);
			})
			.fail(function(){
				self.$el.html('<em class="block it center">'+i18n.t('something went wrong, please try again later!')+'</em>');
			})
			.always(function(){
				app.pageloader.hide();
			});
		}
	});
}(jQuery));
