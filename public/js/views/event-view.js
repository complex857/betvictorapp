/*jslint continue: true, debug: false, eqeq: true, regexp: true, sloppy: true, vars: true, white: true, browser: true, devel: true, indent: 4, es5: false, nomen: true */
/*global jQuery, Backbone, _, $, TableBuilder, i18n */

window.app = window.app || {};

(function ($) {
	'use strict';

	var app = window.app;

	app.EventView = Backbone.View.extend({
		el: $('#content'),
		sport_id: null,
		events: null,
		sport: null,

		initialize: function(sport_id){
			var self = this;
			self.sport_id = sport_id;
			self.events = new app.Events();
			self.render();
		},

		render: function(){
			var self = this;

			app.pageloader.show();

			$.get('/sports/'+self.sport_id, {lang: app.langselect.selected_lang}).done(function(resp){
				_.each(resp.events, function(event_data){
					self.events.push(new app.Event(event_data));
				});
				self.sport = new app.Sport(resp.sport);

				app.breadcrumbs.render('events', {
					sport_id: self.sport.get('id'),
					sport_name: self.sport.get('title')
				});

				var table_builder = new TableBuilder({
					data: self.events.models,
					headers: 'auto',
					formatters: {
						'outcomes': function(event) {
							return '<a class="block" href="#/sports/'+self.sport_id+'/events/'+event.get('id')+'">'+i18n.t('outcomes')+'</a>';
						},
						'time': self.make_date_formatter('time'),
						'start_time': self.make_date_formatter('start_time')
					}
				});
				var table_html = table_builder.build();

				var html =
					'<div class="table-responsive">'+
						'<table id="events" class="table table-condensed table-bordered table-hover">'+
						table_html+
						'</table>'+
					'</div>';

				self.$el.html(html);
			})
			.fail(function(){
				self.$el.html('<em class="block it center">'+i18n.t('something went wrong, please try again later!')+'</em>');
			})
			.always(function(){
				app.pageloader.hide();
			});
		},

		make_date_formatter: function(key){
			return function(model){
				var d = new Date();
				d.setTime(model.get(key));
				return d.toUTCString();
			};
		}
	});
}(jQuery));
