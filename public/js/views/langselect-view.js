/*jslint continue: true, debug: false, eqeq: true, regexp: true, sloppy: true, vars: true, white: true, browser: true, devel: true, indent: 4, es5: false, nomen: true */
/*global jQuery, $, Backbone, _ */

window.app = window.app || {};

$(function(){
	'use strict';

	var app = window.app;
	app.LangSelectView = Backbone.View.extend({
		el: $('#langselect'),
		languages: [
			['en', 'English'],
			['de', 'Deutsch']
		],
		selected_lang: 'en',

		events: {
			'change': 'changeLang'
		},

		initialize: function(){
			this.selected_lang = window.BACKEND_LANG;
			this.render();
		},

		render: function(){
			var self = this;

			var options = [];
			_.each(self.languages, function(lang){
				options.push('<option '+(lang[0] == self.selected_lang ? 'selected' : '')+' value="'+lang[0]+'">'+lang[1]+'</option>');
			});
			self.$el.html(options.join(''));
			return self;
		},

		changeLang: function() {
			var lang = this.$el.val();
			window.location = '/'+lang+'#'+Backbone.history.fragment;
		}
	});
});
