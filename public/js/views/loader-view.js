/*jslint continue: true, debug: false, eqeq: true, regexp: true, sloppy: true, vars: true, white: true, browser: true, devel: true, indent: 4, es5: false, nomen: true */
/*global jQuery, $, Backbone, _ */

window.app = window.app || {};

$(function(){
	'use strict';

	var app = window.app;
	app.LoaderView = Backbone.View.extend({
		el: $('#page_loader'),

		// initialize: function(){
		// },

		show: function(){
			this.$el.addClass('shown');
		},

		hide: function(){
			this.$el.removeClass('shown');
		}
	});
});
