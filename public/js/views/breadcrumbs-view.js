/*jslint continue: true, debug: false, eqeq: true, regexp: true, sloppy: true, vars: true, white: true, browser: true, devel: true, indent: 4, es5: false */
/*global jQuery, $, Backbone, i18n */

window.app = window.app || {};

$(function () {
	'use strict';

	var app = window.app;
	app.BreadcrumbsView = Backbone.View.extend({
		el: $('#breadcrumbs'),

		render: function(route, options){
            options = options || {};
            var html = [];

            html.push(
                '<li class="'+(route == 'default' ? 'active' : '')+'">'+
                '<a href="#">'+i18n.t('home')+'</a>'+
                '</li>'
            );
            if (route == 'events' || route == 'outcomes') {
                html.push(
                    '<li class="'+(route == 'events' ? 'active' : '')+'">'+
                    (route == 'events' ?
                        '<span>'+options.sport_name+' - '+i18n.t('events')+'</span>'
                        :
                        '<a href="#sports/'+options.sport_id+'">'+options.sport_name+' - '+i18n.t('events')+'</a>'
                    )+
                    '</li>'
                );
            }
            if (route == 'outcomes') {
                html.push(
                    '<li class="active">'+
                    '<span>'+options.event_name+' - '+i18n.t('outcomes')+'</span>'+
                    '</li>'
                );
            }
			this.$el.html(html.join(''));
            return this;
		}
	});
});
