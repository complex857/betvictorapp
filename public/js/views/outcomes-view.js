/*jslint continue: true, debug: false, eqeq: true, regexp: true, sloppy: true, vars: true, white: true, browser: true, devel: true, indent: 4, es5: false, nomen: true */
/*global jQuery, Backbone, _, $, TableBuilder, i18n  */

window.app = window.app || {};

(function ($) {
	'use strict';

	var app = window.app;

	app.OutcomesView = Backbone.View.extend({
		el: $('#content'),
		sport_id: null,
		event_id: null,
		outcomes: null,
		sport: null,
		event: null,

		initialize: function(sport_id, event_id){
			var self = this;
			self.sport_id = sport_id;
			self.event_id = event_id;
			self.outcomes = new app.Outcomes();
			self.render();
		},

		render: function(){
			var self = this;

			app.pageloader.show();

			$.get('/sports/'+self.sport_id+'/events/'+self.event_id, {lang: app.langselect.selected_lang}).done(function(resp){
				self.sport = new app.Sport(resp.sport);
				self.event = new app.Event(resp.event);

				_.each(resp.outcomes, function(outcome_data){
					self.outcomes.push(new app.Outcome(outcome_data));
				});

				app.breadcrumbs.render('outcomes', {
					sport_id: self.sport_id,
					event_id: self.event_id,
					sport_name: self.sport.get('title'),
					event_name: self.event.get('title')
				});

				var table_builder = new TableBuilder({
					data: self.outcomes.models,
					headers: 'auto',
					formatters: {}
				});
				var table_html = table_builder.build();

				var html =
					'<div class="table-responsive">'+
						'<table id="events" class="table table-condensed table-bordered table-hover">'+
						table_html+
						'</table>'+
					'</div>';

				self.$el.html(html);
			})
			.fail(function(){
				self.$el.html('<em class="block it center">'+i18n.t('something went wrong, please try again later!')+'</em>');
			})
			.always(function(){
				app.pageloader.hide();
			});

			this.$el.html('');
		}
	});
}(jQuery));
