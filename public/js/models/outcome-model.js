/*global jQuery, $, Backbone, app */
/*jslint continue: true, debug: false, eqeq: true, regexp: true, sloppy: true, vars: true, white: true, browser: true, devel: true, indent: 4, es5: false */

window.app = window.app || {};

$(function () {
	'use strict';

	var Outcome;
	Outcome = window.app.Outcome = Backbone.Model.extend({});

	window.app.Outcomes = Backbone.Collection.extend({
		model: Outcome
	});
});
