/*global jQuery, $, Backbone, app */
/*jslint continue: true, debug: false, eqeq: true, regexp: true, sloppy: true, vars: true, white: true, browser: true, devel: true, indent: 4, es5: false */

window.app = window.app || {};

$(function () {
	'use strict';

	var Event;
	Event = window.app.Event = Backbone.Model.extend({});

	window.app.Events = Backbone.Collection.extend({
		model: Event
	});
});
