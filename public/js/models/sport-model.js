/*global jQuery, $, Backbone, app */
/*jslint continue: true, debug: false, eqeq: true, regexp: true, sloppy: true, vars: true, white: true, browser: true, devel: true, indent: 4, es5: false */

window.app = window.app || {};

$(function () {
	'use strict';

	var Sport;
	Sport = window.app.Sport = Backbone.Model.extend({});

	window.app.Sports = Backbone.Collection.extend({
		model: Sport
	});
});
