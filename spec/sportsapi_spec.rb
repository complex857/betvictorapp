require File.expand_path '../spec_helper.rb', __FILE__

describe "SportsApi class" do

  it "should respond with sports data in order of their 'pos' property" do
    api = make_stubbed_sports_api('sports.json')

    sports = api.sports[:ordered]
    expect(sports.length).to eql(4)

    # check ordering
    expect(sports[0]['pos']).to be <= sports[1]['pos']
    expect(sports[1]['pos']).to be <= sports[2]['pos']
    expect(sports[2]['pos']).to be <= sports[3]['pos']
  end

  it "should respond with sports data indexed by their 'id' property" do
    api = make_stubbed_sports_api('sports.json')

    sports_by_id = api.sports[:by_id]

    expect(sports_by_id[   100]['title']).to eql('Football')
    expect(sports_by_id[   600]['title']).to eql('Tennis')
    expect(sports_by_id[  1200]['title']).to eql('Volleyball')
    expect(sports_by_id[601600]['title']).to eql('Basketball')
  end

  it "the returned sports hashes should have an events_by_id key in them with events indexed by their 'id' property" do
    api = make_stubbed_sports_api('sports_with_events.json')

    sports_by_id = api.sports[:by_id]
    football = sports_by_id[100]

    expect(football.has_key? 'events_by_id').to be true
    expect(football['events_by_id'][254865910]['title']).to eql('Melbourne City v Melbourne Victory')
  end

  it "the returned sports hashes should their nested events be sorted by their 'pos' property" do
    api = make_stubbed_sports_api('sports_with_events.json')

    events = api.sports[:ordered][0]['events']

    expect(events[0]['pos']).to be <= events[1]['pos']
    expect(events[1]['pos']).to be <= events[2]['pos']
    expect(events[2]['pos']).to be <= events[3]['pos']
  end

  it "should not fail chache_valid? before the first request" do
    api = SportsApi.new('')

    expect(api.send(:cache_valid?)).to be false

  end

  it "should consider the previous response valid if it's expires header is in the future" do
    api = SportsApi.new('')

    future_date = (DateTime.now.to_time + 3600).to_datetime
    api.instance_eval do
      @cache['headers'] = {}
      @cache['headers']['expires'] = [future_date.to_s]
    end

    expect(api.send(:cache_valid?)).to be true

  end

  it "should consider the previous response valid if it have been acquired in less than 10 seconds ago" do
    api = SportsApi.new('')

    five_seconds_ago = (DateTime.now.to_time - 5).to_datetime
    api.instance_eval do
      @cache['headers'] = {}
      @cache['last_fetch'] = five_seconds_ago
    end

    expect(api.send(:cache_valid?)).to be true
  end

end
