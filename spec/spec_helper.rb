require 'rack/test'
require 'rspec'
require 'simplecov'

SimpleCov.start

ENV['RACK_ENV'] = 'test'

require File.expand_path '../../betvictor.rb', __FILE__

module RSpecMixin
  include Rack::Test::Methods

  def app
    BetVictorTestApp
  end

end

RSpec.configure { |c| c.include RSpecMixin }

class HTTPResponseStub
  attr_accessor :header
  attr_accessor :body

  def initialize(header, body)
    @body = body
    @header = header
  end

  def to_hash
    @header
  end

  def self.from_file(path, header = {})
    self.new(
      header,
      File.open(path).read
    )
  end
end

def make_stubbed_sports_api(fixture_path)
    api = SportsApi.new('')
    allow(api).to receive(:fetch) do
      HTTPResponseStub.from_file(File.expand_path('../fixtures/' + fixture_path, __FILE__))
    end
    allow(api).to receive(:"cache_valid?") do
      false
    end

    api
end
