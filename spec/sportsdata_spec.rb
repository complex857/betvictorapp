require File.expand_path '../spec_helper.rb', __FILE__

describe "SportsData class" do

  it "should respond with sports data without events" do
    api = make_stubbed_sports_api('sports_with_events.json')
    sports_data = SportsData.new(api)

    sports = sports_data.sports

    expect(sports[0].has_key? 'events').to be false
    expect(sports[0].has_key? 'events_by_id').to be false
    expect(sports[0]['title']).to eql('Football')
  end

  it "should respond with events data given a sport id" do
    api = make_stubbed_sports_api('sports_with_events.json')
    sports_data = SportsData.new(api)

    events = sports_data.events(100)

    expect(events).to be_a Array
    expect(events.length).to eql(4)
    expect(events[0]['title']).to eql('Mohammedan Sc v Fateh Hyderabad')
  end

  it "should accept string sport_id as far as it can be parsed as int" do
    api = make_stubbed_sports_api('sports_with_events.json')
    sports_data = SportsData.new(api)

    events_by_int_id = sports_data.events(100)
    events_by_string_id = sports_data.events("100")

    expect(events_by_int_id).to eql(events_by_string_id)
  end

  it "should fail with error when non-existsing sport_id is asked for" do
    api = make_stubbed_sports_api('sports_with_events.json')
    sports_data = SportsData.new(api)

    no_such_sport_id = 42
    expect { sports_data.events(no_such_sport_id) }.to raise_error SportNotFound
  end

  it "should return outcomes of an event given it's sport id and event id" do
    api = make_stubbed_sports_api('sports_with_events_and_outcomes.json')
    sports_data = SportsData.new(api)

    outcomes = sports_data.outcomes(100, 301904110)

    expect(outcomes).to be_a Array
    expect(outcomes.length).to eql(3)
    expect(outcomes[0]).to be_a Hash
    expect(outcomes[0]['description']).to eql('Mohammedan Sc')
  end

  it "should throw errors if non-existsing sport_id or event_id is asked for" do
    api = make_stubbed_sports_api('sports_with_events_and_outcomes.json')
    sports_data = SportsData.new(api)

    no_such_sport_id = 42
    no_such_event_id = 42
    valid_sport_id = 100
    valid_event_id = 301904110

    expect { sports_data.outcomes(no_such_sport_id, valid_event_id) }.to raise_error SportNotFound
    expect { sports_data.outcomes(valid_sport_id, no_such_event_id) }.to raise_error EventNotFound
  end

  it "should return a single sport by it's id" do
    api = make_stubbed_sports_api('sports_with_events.json')
    sports_data = SportsData.new(api)

    sport = sports_data.sport(100);

    expect(sport['id']).to eql(100)
    expect(sport.has_key? 'events').to be false
    expect(sport.has_key? 'events_by_id').to be false
  end

  it "should return a single event by it's own id and the sport's id it belongs to" do
    api = make_stubbed_sports_api('sports_with_events.json')
    sports_data = SportsData.new(api)

    event = sports_data.event(100, 301904110);

    expect(event['id']).to eql(301904110)
    expect(event['title']).to eql("Mohammedan Sc v Fateh Hyderabad")
  end

end
