require File.expand_path '../spec_helper.rb', __FILE__

describe "BetVictor Interview Task App" do

  it "should allow return the frontend on root" do
    get '/'

    expect(last_response).to be_ok
    expect(last_response).to match(/<title>\s*BetVictor Interview Task\s*<\/title>/)
  end

  it "should transfer language from the path to javascript" do
    get '/en'
    expect(last_response).to match(/BACKEND_LANG\s*=\s*'en'/)

    get '/de'
    expect(last_response).to match(/BACKEND_LANG\s*=\s*'de'/)
  end

end
